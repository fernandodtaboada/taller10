#!/bin/bash
sudo apt-get update
sudo apt-get install apache2 -y 
sudo apt-get install libapache2-mod-php7.2 -y 
sudo apt-get install php7.2-mysql -y 
sudo apt-get install mariadb-server mariadb-client -y
sudo wget http://wordpress.org/latest.tar.gz 
sudo tar xzvf latest.tar.gz
sudo cp -avr wordpress/* /var/www/html/
sudo chown -R www-data:www-data /var/www/html/*
sudo rm -rf /var/www/html/index.html 
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
sudo /etc/init.d/apache2 restart
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service
sudo systemctl restart mariadb.service
sudo sed -i 's/127.0.0.1/0.0.0.0/g' "/etc/mysql/mariadb.conf.d/50-server.cnf"
sudo systemctl restart mariadb.service
IP=$(GET http://169.254.169.254/latest/meta-data/local-ipv4/)
sudo sed -i 's/localhost/'$IP'/g' "/var/www/html/wp-config.php"
sudo sed -i 's/username_here/wordpress/g' "/var/www/html/wp-config.php"
sudo sed -i 's/password_here/wordpress,,123/g' "/var/www/html/wp-config.php"
sudo sed -i 's/database_name_here/wordpress/g' "/var/www/html/wp-config.php"
sudo mysql < bd.sql


